// SETTINGS
const cuentasCtrl = {};
const mongoose = require('mongoose')

// DATABASE MODELS 
const Cuenta = require("../models/Cuenta"); //constante note para el esquema de la db
const counter = require('../models/Counter')


// ********** ACCOUNTS CONTROLLERS **********

cuentasCtrl.getCuenta = async (req,res) =>{
    if (req.user){ 
        userAccounts = await Cuenta.find({user: req.user.id}).sort({createdAt:'desc'}); //query 
        res.json(userAccounts)
    } else{
        userAccounts = await Cuenta.findOne({}).sort({createdAt:'desc'}); //query user: req.user.id
        res.json(userAccounts)
        // res.status(404).send('Requiere autenticar su acceso')
    }

}

cuentasCtrl.createCuenta =  async (req,res) => {       //usar una promesa pendiente
    movimientos=[]
    const numero = await Cuenta.countDocuments({},function(err, c) {console.log('Count is ' + c);   });
    const user = req.user.id;
    const newCuenta = new Cuenta({
                    user: user,
                    idCliente : numero+1,
                    movements : movimientos,    
                    accountNumber: numero
    });
    console.log(numero)
    if(numero!=0){
        res.send('Ya tienes una cuenta') 
    }else{
        newCuenta.save(); //guardar objeto en mongoDB con mongoose
        res.send('Cuenta creada exitosamente');
    }
}

cuentasCtrl.updateCuenta = (req,res) =>{
    res.send('actualizando cuenta')
    // const { title, description } = req.body;
    // await Note.findByIdAndUpdate(req.params.id, { title, description });
    }

cuentasCtrl.deleteCuenta = async (req,res) => {
    userAccounts = await Cuenta.remove({user: req.user.id})
    res.send('eliminando cuenta')
    // await Note.findByIdAndDelete(req.params.id);
}

// ********** MOVEMENTS CONTROLLERS **********

cuentasCtrl.createMove = async (req, res) =>{

    const numero = await Cuenta.countDocuments({'movements.0':{$exists:true}} );
    var num
    if(numero==0){
        num=0
    }else{
        numIds = await Cuenta.aggregate([
            {$unwind:'$movements'},
            {$group:{"_id" : null,"maxId" : {"$max" : "$movements._id"}}}   ]);
        num = numIds[0].maxId+1;
    }

    var value = req.body.value;
    var type = req.body.type;
    var move = {'_id':num,'type':type,'value':value}

    userAccount = await Cuenta.updateOne(
        {},
        {'$push':{movements:move}}
    );
    res.json(move)
}

cuentasCtrl.getMoves= async (req, res) =>{
    const moves= await Cuenta.aggregate([
        {$project:{'movements':1,'_id':0}}
        ])
    res.json(moves[0])
}

cuentasCtrl.updateMove= async (req, res) =>{
    var strId =req.params.id;
    var Id = parseInt(strId.substring(1));
    var value = req.body.value;
    var type = req.body.type;

    dict = await Cuenta.updateOne({'movements._id':Id},
                {$set:{
                    'movements.$.value':value,
                    'movements.$.type':type
                }})
    
    res.json(dict)
}

cuentasCtrl.deleteMove= async (req, res) =>{
    var strId =req.params.id;
    var Id = parseInt(strId.substring(1));
    if(Id){
        movs = await Cuenta.findOneAndUpdate({},
            {$pull:{
                movements:{'_id':Id}
            }})
        res.json(movs)
    }
    else{
        res.status(400)
    }
    
}

module.exports = cuentasCtrl; 


//controlador
const usersCtrl = {};
//esquemas
const Usuario = require('../models/Usuario');
//passport
const passport = require('passport')

// SIGNUP solo genera nuevo usuario
usersCtrl.signUp = async (req,  res) => {
    const name = req.body.name;
    const surname = req.body.surname;
    const email = req.body.email;
    const password = req.body.password;

    //ver si ya existe el usuario
    const contador = await Usuario.countDocuments({}) +1;
    const emailUser = await Usuario.findOne({ name: name, surname: surname })

    if (emailUser) {
        res.status(409).send('Usuario ya registrado')

    } else {
        const newUser = new Usuario({ name: name, surname: surname, email: email, password: password, _idCliente:contador});
        newUser.password = await newUser.encryptPassword(password)
        //registrando nuevo usuario
        await newUser.save(function (err, newUser) {
            if (err) return console.err();
        })

        await res.json({email:email,message:"usuario dado de alta"})
    }
}

// LOGIN CON SESION DE PASSPORT
usersCtrl.signIn = passport.authenticate("local", {
    successRedirect: "/successjson",
    failureRedirect: "/failurejson"
});

usersCtrl.successAuth = async function(req,res){
    const info = req.session
    console.log(info)
    console.log(req.session.passport)
    res.json(info)
}

usersCtrl.failureAuth = function(req,res){
    res.status(400).send('Credenciales incorrectas');
}

// LOGOUT
usersCtrl.logOut = (req, res) => {
    req.logout();
    res.send('cerrando session');
}

//getUserData
usersCtrl.userData = async function(req,res){//_id:req.user.id
    userAccount = await Usuario.findOne({},{name:1,surname:1,email:1,createdAt:1})


    res.json(userAccount)
}

module.exports = usersCtrl;


//Rutas de usuarios
const { Router } = require('express');
const router = Router();

////Authenicate with passport to protect the urls 
const {isAuthenticated} = require('../config/auth')

// Que podemos hacer con las cuentas, definicion d elos callbacks
const {  
    signUp, 
    signIn, 
    successAuth,
    failureAuth,
    logOut,
    userData
    } = require('../controllers/user.controller');

////Callbacks

//Sign Up - Registrate
router.post('/Signup', signUp); //devuelve formulario

//Log In - Acceso
router.post('/Login', signIn); //devuelve formulario

router.get('/successjson', successAuth);

router.get('/failurejson', failureAuth);

//Log Out - Salir
router.get('/Logout', logOut); //devuelve formulario

//getDataUser
router.get('/userData',userData);

module.exports = router;




//Rutas de cuentas y movimientos

const { Router } = require('express');
const router = Router();

////Authenicate with passport to protect the urls 
const {isAuthenticated} = require('../config/auth')

// ********* ACCOUNTS ROUTES **********
// CALLBACKS
// Que podemos hacer con las cuentas, definicion de los callbacks
const {  
    createCuenta, 
    getCuenta,
    updateCuenta, 
    deleteCuenta
    } = require('../controllers/cuenta.controller');


// Nueva cuenta, isAuthenticated
router.post('/cuentas', createCuenta); //devuelve formulario

// Consultar Cuenta
router.get('/getCuentas', getCuenta); //devuelve formulario

// Editar Cuenta
router.put('/cuentas', updateCuenta); //devuelve formulario

//Borrar cuenta
router.delete('/cuentas', deleteCuenta); //devuelve formulario


// ********* MOVEMENTS ROUTES **********
// CALLBACKS
const {createMove, getMoves, updateMove, deleteMove} = require('../controllers/cuenta.controller');

router.post('/movements', createMove);
router.get('/movements', getMoves);
router.put('/movements/edit:id', updateMove);//isAuthenticated
router.delete('/movements/delete:id', deleteMove);
module.exports = router;





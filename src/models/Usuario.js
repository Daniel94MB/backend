const mongoose = require('mongoose');
const bcrypt = require('bcryptjs')

const UserEsquema = new mongoose.Schema({
    _idCliente: {type: Number},
    name: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true
    }
},{
    timestamps:true
})

UserEsquema.methods.speak = function () {
    const info = this.name
        ? "mi nombre es " + this.name
        : "no tengo nombre"
    console.log("schemaUser working")
}

UserEsquema.methods.encryptPassword = async password => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
};

UserEsquema.methods.matchPassword = async function (password) {
    return await bcrypt.compare(password, this.password);
};

module.exports = mongoose.model('Usuario', UserEsquema)





const mongoose = require('mongoose');

const movementsEsquema = new mongoose.Schema({
    _id: Number, 
    value: Number,
    type: String,
    date: { type: Date, required: true, default: Date.now}
})

const CuentaEsquema = new mongoose.Schema({

    idCuenta: {type: Number},
    movements:[movementsEsquema],
    user:{
        type: String,
        required: true 
    },
    accountNumber:{
        type: Number,
        required: true}
},{
    timestamps:true
})

module.exports = mongoose.model('Cuenta', CuentaEsquema)

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/Usuario');

passport.use(new LocalStrategy({usernameField: 'email'}, 
    async function (email, password, done){
      await User.findOne({email: email}, async function (err,user){
        console.log(user)
        if (err) { 
          console.log(err)
          return done(err)}
        if (!user){
          console.log('no se autentico en passport')
          return done(null, false, {message: 'Incorrect email.'});
        }
        const match = await user.matchPassword(password);
        console.log('match')
        console.log(match)
        if(!match){
          return done(null, false, {message: 'Incorrect password'})
        }
          console.log('si se autentico en passport')
          return done(null,user);  

      });
    }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});


//////////////

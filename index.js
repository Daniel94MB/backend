
require('dotenv').config()
const app = require("./server")
require('./database') // for mongoDB

//console.log(process.env.TESTING)

// Prueba de conexion del servidor
app.listen(app.get('port'),()=>{
    console.log("server on port: ",app.get('port'));
    console.log('Environment:', process.env.NODE_ENV);
})


// Inicializacion de variables
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const requestjson = require('request-json');
const bodyparser = require('body-parser');
const session = require('express-session');
// const connectMongo = require('connect-mongo');
const passport = require('passport');
const mongoose = require('mongoose')
var cors = require('cors');

// //inicializacion de variable
const app = express();
require('./src/config/passport')

// // Configuracion de puerto de conexion
app.set('port',process.env.PORT || 3000);

////// CORS
app.use(cors());
app.use(bodyparser.json());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', req.headers.origin || '*');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
  res.header("Access-Control-Allow-Credentials", "true");
  next();
})

// // //Middlewares 
app.use(morgan('dev')); //quitar en prod
app.use(express.urlencoded({extended: false}));
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
}));
app.use(passport.initialize());
app.use(passport.session());

// Global Variables
app.use((req, res, next) => {
  res.locals.user = req.user || null;
  next();
})


// // //Routes
//app.use(require('./src/routes/login.routes'));
//  app.use(require('./routes/index.routes'));
app.use(require('./src/routes/cuentas.routes'));
app.use(require('./src/routes/users.routes'));

// // //Static Files
app.use(express.static(path.join(__dirname,'public')));

module.exports = app;

// // // // API ************

// // // ////Esto no se para que es

// // // 


// // // var movimientosJSON = require('./movimientosv2.json')

// // // console.log('todo list RESTful API server started on: ' + port);

// // // app.post('/todo', function(req,res){
// // //   console.log("succesfully triggered a post")
// // // })

// // // app.get('/', function(req, res) {
// // //   res.sendFile(path.join(__dirname, 'index.html'))
// // // })

// // // app.post('/', function(req, res) {
// // //   res.send("Hemos recibido su peticion post")
// // // })

// // // app.put('/', function(req, res) {
// // //   res.send("Hemos recibido su peticion put cambiada")
// // // })

// // // app.delete('/', function(req, res) {
// // //   res.send("Hemos recibido su peticion delete")
// // // })

// // // app.get('/Clientes/:idcliente', function(req, res) {
// // //   res.send("Aqui tiene al cliente número: " + req.params.idcliente);
// // // })

// // // app.get('/v1/Movimientos', function(req, res) {
// // //   res.sendfile('movimientosv1.json')
// // // })

// // // app.get('/v2/Movimientos', function(req, res) {
// // //   res.json(movimientosJSON)
// // // })

// // // app.get('/v2/Movimientos/:idcliente', function(req, res) {
// // //   res.send(movimientosJSON[req.params.idcliente-1]);
// // // })

// // // app.get('/v2/Movimientosquery', function(req, res) {
// // //   res.send(req.query)
// // // })

// // // app.post('/v2/Movimientos', function(req, res) {
// // //   var nuevo = req.body
// // //   nuevo.id = movimientosJSON.length + 1
// // //   movimientosJSON.push(nuevo)
// // //   res.send("Movimiento dado de alta")
// // // })

// // // app.get('/Clientes', function(req, res) {
// // //   clienteMLab.get('', function(err, resM, body) {
// // //     if (!err) {
// // //       res.send(body);
// // //     } else {
// // //       console.log(body);
// // //     }
// // //   })
// // // })

// // // app.post('/Clientes', function(req, res) {
// // //   clienteMLab.post('', req.body, function(err, resM, body) {
// // //       res.send(body);
// // //   })
// // // })
